package mastermind;

import org.junit.Assert;
import org.junit.Test;

import java.io.Serializable;

public class MasterMindTests  {

    private void testEvaluation(String secret, String guess, int rightPosition, int wrongPosition) {
        Evaluation expected = new Evaluation(rightPosition, wrongPosition);
        Evaluation evaluation = Evaluation.evaluation(secret, guess);

        Assert.assertEquals(String.format("Wrong evaluation for secret=%s, guess=%s", secret, guess), expected, evaluation);
    }

    @Test
    public void testEqual() {
        testEvaluation("ABCD", "ABCD", 4, 0);
    }

    @Test
    public void testOnlyWrongPositions() {
        testEvaluation("DCBA", "ABCD", 0, 4);
    }

    @Test
    public void testSwap() {
        testEvaluation("ABCD", "ABDC", 2, 2);
    }

    @Test
    public void testRightPositions() {
        testEvaluation("ABCD", "ABCF", 3, 0);
    }

    @Test
    public void testWrongPositions() {
        testEvaluation("DAEF", "FECA", 0, 3);
    }

    // repeated letters

    @Test
    public void testRightPosition() {
        testEvaluation("AABC", "ADFE", 1, 0);
    }

    @Test
    public void testSameLetters() {
        testEvaluation("AABC", "DEAA", 0, 2);
    }

    @Test
    public void testSameGuesMoreLetters() {
        testEvaluation("ACBC", "DEAA", 0, 1);
    }

}
