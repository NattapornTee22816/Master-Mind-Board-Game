package mastermind;

import java.io.Serializable;

public class Evaluation {

    private int rightPosition;
    private int wrongPosition;

    public Evaluation(int rightPosition, int wrongPosition) {
        this.rightPosition = rightPosition;
        this.wrongPosition = wrongPosition;
    }

    public static Evaluation evaluation(String secret, String guess) {
        /* SAMPLE CODE */
        /* REMOVE THIS CODE WHEN USE TEST */
        int rightPosition = 0;
        int wrongPosition = 0;

        String secretTmp = new String(secret);
        for (int i = 0; i < guess.length(); i++) {
            if (guess.charAt(i) == secretTmp.charAt(i)) {
                rightPosition++;
                secretTmp = secretTmp.replaceFirst(Character.toString(guess.charAt(i)), "-");
                guess = guess.replaceFirst(Character.toString(guess.charAt(i)), "-");
            }
        }

        for (int i = 0; i < guess.length(); i++) {
            if (guess.charAt(i) == '-') {
                continue;
            }
            if (secretTmp.indexOf(Character.toString(guess.charAt(i))) != -1) {
                wrongPosition++;
                secretTmp = secretTmp.replaceFirst(Character.toString(guess.charAt(i)), "-");
            }
        }

        return new Evaluation(rightPosition, wrongPosition);
        // TODO
//         return null;
    }

    /* GETTER SETTER */

    public Boolean isCompleted(int inputLength) {
        return this.rightPosition == inputLength;
    }

    public int getRightPosition() {
        return rightPosition;
    }

    public void setRightPosition(int rightPosition) {
        this.rightPosition = rightPosition;
    }

    public int getWrongPosition() {
        return wrongPosition;
    }

    public void setWrongPosition(int wrongPosition) {
        this.wrongPosition = wrongPosition;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Evaluation) {
            Evaluation comp = (Evaluation) obj;
            if (this.rightPosition != comp.rightPosition) {
                return false;
            }
            if (this.wrongPosition != comp.wrongPosition) {
                return false;
            }
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return String.format("[rightPosition = %d, wrongPosition = %d]", this.rightPosition, this.wrongPosition);
    }

}
