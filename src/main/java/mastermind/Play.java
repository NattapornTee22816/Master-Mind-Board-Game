package mastermind;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Play {

    private static int CODE_LENGTH = 4;
    private static String ACCEPT_ALPHABET = "[ABCDEF]{4}";

    public static void main(String[] args) {
        playMasterMind();
    }

    private static void playMasterMind() {
        Scanner sc = new Scanner(System.in);
        String secret = generateSecret();
        Evaluation evaluation;
        System.out.println(secret);

        System.out.print("Your guess: ");
        do {
            String guess = sc.next();

            while (hasErrorInInput(guess)) {
                System.out.print("Incorrect input: " + guess + ". " +
                        "It should consist of " + CODE_LENGTH  + " digits and character in range 'A'..'F'. " +
                        "Try again.");
                guess = sc.next();
            }

            evaluation = Evaluation.evaluation(secret, guess);
            if (evaluation.isCompleted(CODE_LENGTH)) {
                System.out.println("You are correct!");
            } else {
                System.out.print(String.format("Right Positions: %d, Wrong Position: %d. Try again.", evaluation.getRightPosition(), evaluation.getWrongPosition()));
            }
        } while (!evaluation.isCompleted(CODE_LENGTH));
    }

    private static Boolean hasErrorInInput(String guess) {
        return !guess.matches(ACCEPT_ALPHABET);
    }

    private static String generateSecret() {
        List<String> alphabet = Arrays.asList("A", "B", "C", "D", "E", "F");
        Random random = new Random();
        String generate = "";
        for (int i = 0; i < CODE_LENGTH; i++) {
            generate += alphabet.get(random.nextInt(alphabet.size()));
        }
        return generate;
    }

}
